# Piping Editor

Text editor based on pipng information between independent modules. Core written in low-level Rust provides a robust base. JSON-RPC communication allows for multi-language, multi-platform solutions.

# Goals

The main goal to this project is simplicity. Text editor should edit text, not check your email. Every new feature will be implemented as separate, independent module so every user can build their own editor: from simple to full blown IDE.

Sign