use piped_protocol::RPC;

#[derive(Debug, Clone)]
pub struct Message {
    pub sender: String,
    pub data: RPC,
}

impl Message {
    pub fn new(sender: &str, data: RPC) -> Self {
        Message {
            sender: sender.to_owned(),
            data,
        }
    }
}
