use bytes::{BufMut, BytesMut};
use piped_protocol::{RPCReader, Result};
use tokio::{
    io::{stdin, stdout, AsyncWriteExt},
    sync::mpsc::{Receiver, Sender},
    try_join,
};

use crate::message::Message;

async fn reader(tx: Sender<Message>) -> Result<()> {
    let mut rpc = RPCReader::new();
    loop {
        match rpc.read_input(&mut stdin()).await {
            Ok(Some(message_data)) => tx.send(Message::new("stdio", message_data)).await?,
            Ok(None) => return Ok(()),
            Err(error) => {
                eprintln!("{error}");
                rpc.reset();
                continue
            },
        }
    }
}

async fn writter(mut rx: Receiver<Message>) -> Result<()> {
    while let Some(message) = rx.recv().await {
        let mut log = BytesMut::new();
        log.put_slice(b"[");
        log.put_slice(message.sender.as_bytes());
        log.put_slice(b"]: (");
        /*log.put_slice(message.data.path.as_bytes());
        log.put_slice(b") ");
        log.put_slice(&message.data.payload);*/
        log.put_slice(b"\n");
        //handle_output(log.freeze(), &mut stdout()).await?;
        stdout().write_buf(&mut log).await?;
    }

    Ok(())
}

pub async fn run(tx: Sender<Message>, rx: Receiver<Message>) -> Result<()> {
    try_join!(reader(tx), writter(rx)).unwrap();

    Ok(())
}
