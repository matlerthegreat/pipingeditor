mod message;
mod subsystem;
mod utils;

use std::collections::HashMap;
use std::error::Error;

use futures::Future;
use message::Message;
use tokio::{
    sync::mpsc::{self, Receiver, Sender},
    task::JoinHandle,
};

fn spawn_subsystem<T>(
    tx: Sender<Message>,
    subsystem: fn(Sender<Message>, Receiver<Message>) -> T,
) -> (JoinHandle<T::Output>, Sender<Message>)
where
    T: Future + Send + 'static,
    T::Output: Send + 'static,
{
    let (subsystem_tx, subsystem_rx) = mpsc::channel::<Message>(64);
    (tokio::spawn(subsystem(tx, subsystem_rx)), subsystem_tx)
}

#[tokio::main(flavor = "current_thread")]
async fn main() -> Result<(), Box<dyn Error>> {
    // Subsystems send messages to tx for them to be recieved on rx
    let (tx, mut rx) = mpsc::channel::<Message>(64);

    let mut subsystems = HashMap::new();

    subsystems.insert(
        "stdio".to_string(),
        spawn_subsystem(tx.clone(), subsystem::stdio::run),
    );
    subsystems.insert(
        "child".to_string(),
        spawn_subsystem(tx, subsystem::child::run),
    );

    while let Some(message) = rx.recv().await {
        match message.data.method.as_str() {
            /*scope if scope.starts_with("subsystems")  => {
                if scope.ends_with("register") {
                    let params: SubsystemsRegisterParams = serde_json::from_value(message.data.params.unwrap())?;
                }
            }
            scope => {
                if let Some((_, subsystem_tx)) = scope_map.get(scope).and_then(|subsystem| subsystems.get(subsystem)) {
                    subsystem_tx.send(message).await?
                }
            }*/
            _ => {
                for (_, subsystem_tx) in subsystems.values() {
                    subsystem_tx.send(message.clone()).await?
                }
            }
        }
    }

    for (handle, subsystem_tx) in subsystems.into_values() {
        drop(subsystem_tx);
        let inner = handle.await?;
        inner?
    }

    Ok(())
}
