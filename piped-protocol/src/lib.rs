use bytes::{Buf, BufMut, Bytes, BytesMut};
use http::{
    header::{HeaderName, CONTENT_LENGTH, CONTENT_TYPE},
    HeaderMap, HeaderValue,
};
use httparse::{parse_headers, Status};
use serde::{Deserialize, Serialize};
use std::fmt::Write;
use tokio::io::{AsyncRead, AsyncReadExt, AsyncWrite, AsyncWriteExt};

mod error;
pub use error::{Error, Result};

pub fn get_headers(src: &[u8]) -> Result<Status<(usize, HeaderMap)>> {
    let mut raw_headers = [httparse::EMPTY_HEADER; 4];
    match parse_headers(src, &mut raw_headers) {
        Ok(Status::Complete((n, raw_headers_slice))) => {
            let mut headers = HeaderMap::new();
            for raw_header in raw_headers_slice {
                let header_name = HeaderName::from_bytes(raw_header.name.as_bytes())
                    .map_err(Error::intput_parsing)?;
                let header_value =
                    HeaderValue::from_bytes(raw_header.value).map_err(Error::intput_parsing)?;
                headers.append(header_name, header_value);
            }
            Ok(Status::Complete((n, headers)))
        }
        Ok(Status::Partial) => Ok(Status::Partial),
        Err(error) => Err(Error::intput_parsing(error)),
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct RPCRequest {
    pub method: String,
    pub params: Option<serde_json::Value>,
    pub id: Option<serde_json::Value>
}

pub struct RPCResponse {
    pub result: serde_json::Value,
    pub id: Option<serde_json::Value>,
}

pub struct RPCError {
    pub error: serde_json::Value,
    pub id: Option<serde_json::Value>,
}

pub type RPC = RPCRequest;

pub struct RPCReader {
    buffer: BytesMut
}

impl RPCReader {
    pub fn new() -> Self {
        RPCReader { buffer: BytesMut::new() }
    }

    pub fn reset(&mut self) {
        self.buffer.clear();
    }

    pub async fn read_input(
        &mut self,
        input: &mut (impl AsyncRead + Unpin),
    ) -> Result<Option<RPC>> {
        let (headers_length, headers) = loop {
            match get_headers(&self.buffer[..])? {
                Status::Complete(result) => {
                    break Ok::<_, Error>(result);
                }
                Status::Partial => {
                    if input.read_buf(&mut self.buffer).await? == 0 {
                        return Ok(None);
                    }
                }
            }
        }?;

        eprintln!("BUffer: {:?}", &self.buffer);
    
        self.buffer.advance(headers_length);
    
        let content_length: usize = headers
            .get(CONTENT_LENGTH)
            .ok_or("content_length header missing")
            .map_err(Error::intput_parsing)?
            .to_str()
            .map_err(Error::intput_parsing)?
            .parse()
            .map_err(Error::intput_parsing)?;
    
        let content_type: &str = headers
            .get(CONTENT_TYPE)
            .ok_or("content_type header missing")
            .map_err(Error::intput_parsing)?
            .to_str()
            .map_err(Error::intput_parsing)?;
    
        while self.buffer.len() < content_length {
            if input.read_buf(&mut self.buffer).await.map_err(Error::internal)? == 0 {
                return Ok(None);
            }
        }
    
        eprintln!("content_length: {:?}", content_length);
        eprintln!("BUffer: {:?}", &self.buffer);
        let content_buffer = self.buffer.split_to(content_length);
    
        eprintln!("content_buffer: {:?}", &content_buffer);

        let rpc: RPC = match content_type {
            "application/json" => {
                serde_json::from_slice(&content_buffer).map_err(Error::intput_parsing)
            }
            _ => Err(Error::intput_parsing(
                "content_type header's value unrecognizable",
            )),
        }?;
    
        /*let method: Uri = rpc.method.parse().map_err(Error::intput_parsing)?;
        let scope = method.host();
        let path = method.path();
        let payload = content_buffer.freeze();*/
    
        Ok(Some(rpc))
    }
}

pub async fn write_output(message: RPC, output: &mut (impl AsyncWrite + Unpin)) -> Result<()> {
    let json: serde_json::Value = serde_json::json!(message);
    let mut content = BytesMut::new();
    writeln!(&mut content, "{}", json)?;

    let mut buffer = BytesMut::new();
    writeln!(&mut buffer, "content-length: {}", content.len())?;
    writeln!(&mut buffer, "content-type: application/json")?;
    writeln!(&mut buffer)?;
    buffer.extend(content);
    buffer.put_u8(0);

    while buffer.has_remaining() {
        output.write_buf(&mut buffer).await?;
    }

    Ok(())
}
