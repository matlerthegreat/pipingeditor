use std::error::Error as StdError;
use std::fmt::{self, Debug};

type AsyncError = Box<dyn StdError + Send + Sync>;

#[derive(Debug)]
pub enum Error {
    Internal(AsyncError),
    InputParsing(AsyncError),
}

impl Error {
    pub fn internal(source: impl Into<AsyncError>) -> Self {
        Error::Internal(source.into())
    }
    pub fn intput_parsing(source: impl Into<AsyncError>) -> Self {
        Error::InputParsing(source.into())
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Subsystem error: ")?;
        match &self {
            Error::Internal(inner) => write!(f, "Internal error: {inner}")?,
            Error::InputParsing(inner) => write!(f, "Input parsing error: ({inner})")?,
        }
        Ok(())
    }
}

impl StdError for Error {
    fn source(&self) -> Option<&(dyn StdError + 'static)> {
        match &self {
            Error::Internal(internal) => Some(internal.as_ref() as &(dyn StdError + 'static)),
            Error::InputParsing(internal) => Some(internal.as_ref() as &(dyn StdError + 'static)),
        }
    }
}

impl From<std::io::Error> for Error {
    fn from(other: std::io::Error) -> Self {
        Error::internal(other)
    }
}

impl From<std::fmt::Error> for Error {
    fn from(other: std::fmt::Error) -> Self {
        Error::internal(other)
    }
}

impl<T> From<tokio::sync::mpsc::error::SendError<T>> for Error
where
    T: Debug + Send + Sync + 'static,
{
    fn from(other: tokio::sync::mpsc::error::SendError<T>) -> Self {
        Error::internal(other)
    }
}

impl From<tokio::task::JoinError> for Error {
    fn from(other: tokio::task::JoinError) -> Self {
        Error::internal(other)
    }
}

pub type Result<T> = core::result::Result<T, Error>;
