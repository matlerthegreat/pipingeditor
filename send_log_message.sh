#! /usr/bin/env sh

#while :
#do
echo "content-length: 76"
echo "content-type: application/json"
echo ""
echo -n '{"jsonrpc": "2.0", "method": "piped://stdio/log", "params": "One two three"}'

    read content_length_header
    read content_type_header
    read empty_line
    read content

    #echo "1: $content_length_header" >> out
    #echo "2: $content_type_header" >> out
    #echo "3: $empty_line" >> out
    #echo -n "4: $content" >> out

    output_message=`jq '{jsonrpc, method: "piped://stdio/log", params: "Message from child"}' <<< $content`
    output_length=`wc -c <<< $output_message`
    output_length=`expr $output_length - 1`

    echo "content-length: $output_length"
    echo "content-type: application/json"
    echo ""
    echo -n "$output_message"
#done