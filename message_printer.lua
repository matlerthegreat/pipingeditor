#! /usr/bin/env lua

file = io.open(arg[1], "r")
content = file:read "*a"
content_length = #content

os.execute("sleep 1")

io.flush()
io.write(string.format("content-length: %d\n", content_length))
io.write("content-type: application/json\n")
io.write("\n")
io.write(content)
io.flush()

os.execute("sleep 1")